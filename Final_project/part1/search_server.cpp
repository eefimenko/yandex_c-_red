#include "search_server.h"
#include "iterator_range.h"
#include "profile.h"

#include <algorithm>
#include <iterator>
#include <sstream>
#include <iostream>
#include <vector>

vector<string> SplitIntoWords(const string& line) {
  istringstream words_input(line);
  return {make_move_iterator(istream_iterator<string>(words_input)),
	  make_move_iterator(istream_iterator<string>())};
}

SearchServer::SearchServer(istream& document_input) {
  UpdateDocumentBase(document_input);
}

void SearchServer::UpdateDocumentBase(istream& document_input) {
  InvertedIndex new_index;

  for (string current_document; getline(document_input, current_document); ) {
    new_index.Add(move(current_document));
  }

  index = move(new_index);
}

void SearchServer::AddQueriesStream(
    istream& query_input, ostream& search_results_output)
{
    LOG_DURATION("Add queries");
    vector<size_t> docid_count;
    vector<size_t> docid;
    docid_count.assign(index.getSize(), 0);
    docid.assign(index.getSize(), 0);
    
    for (string current_query; getline(query_input, current_query); ) {
	const auto words = SplitIntoWords(current_query);

	docid_count.assign(index.getSize(), 0);
	for (size_t i = 0; i < docid.size(); ++i)
	{
	    docid[i] = i;
	}
	
	for (const auto& word : words) {
	    for (const auto& id : index.Lookup(word)) {
		docid_count[id.first] += id.second;
	    }
	}
	
	partial_sort(
	    begin(docid),
	    Head(docid,5).end(),
	    end(docid),
	    [&docid_count](size_t lhs, size_t rhs) {
		if (docid_count[lhs] < docid_count[rhs]) return false;
		if (docid_count[rhs] < docid_count[lhs]) return true;
		return lhs < rhs;
	    }
	    );
//      cout << "Here 4" << endl;

	search_results_output << current_query << ':';
	//cout << Head(docid, 5).size() << endl;
	for (auto id : Head(docid, 5)) {
	    //cout << "id=" << id << " " << docid_count[id] << endl;
	    if (docid_count[id] == 0)
	    {
		break;
	    }
	    search_results_output << " {"
				  << "docid: " << id << ", "
				  << "hitcount: " << docid_count[id] << '}';
	}
	search_results_output << endl;
    }
}

void InvertedIndex::Add(const string& document) {
  docs.push_back(document);
  const size_t docid = docs.size() - 1;
  
  for (const std::string& word : SplitIntoWords(document)) 
  {
      auto& count = index[word];
      if (!count.empty() && count.back().first == docid)
      {
	  ++count.back().second;
      }
      else 
      {
	  count.emplace_back(docid, 1);
      }
  }
}

vector<pair<size_t,size_t>>& InvertedIndex::Lookup(const string& word) {
    static vector<pair<size_t,size_t>> res;
    if (auto it = index.find(word); it != index.end()) {
	return it->second;
    } else {
	return res;
    }
}
