#pragma once

#include <algorithm>
//#include <iostream>

using namespace std;

template <typename It>
class IteratorRange {
public:
  IteratorRange(It first, It last) : first(first), last(last) {
  }

  It begin() const {
    return first;
  }

  It end() const {
    return last;
  }

  size_t size() const {
    return last - first;
  }

private:
  It first, last;
};

template <typename Container>
auto Head(Container& c, int top) {
//    cout << min<size_t>(max(top, 0), c.size()) << endl;
  return IteratorRange(begin(c), begin(c) + min<size_t>(max(top, 0), c.size()));
}
