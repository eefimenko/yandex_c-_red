#include "stats.h"
#include "http_request.h"
#include <string_view>
#include <iostream>
#include <string>

using namespace std;

HttpRequest ParseRequest(string_view line)
{
    HttpRequest tmp;
//    cout << "Request: " << line << endl;
    auto pos = line.find_first_not_of(' ');
    line.remove_prefix(pos);
//    cout << "Request 1: " << line << endl;
    pos = line.find_first_of(' ');
    tmp.method = line.substr(0, pos);
    if (pos != string::npos)
    {
	line.remove_prefix(pos+1);
    }
//    cout << "Request 2: " << line << endl;
    pos = line.find_first_not_of(' ');
    line.remove_prefix(pos);
    
    pos = line.find_first_of(' ');
    tmp.uri = line.substr(0, pos);
    if (pos != string::npos)
    {
	line.remove_prefix(pos+1);
    }
    
    pos = line.find_first_not_of(' ');
    line.remove_prefix(pos);
    pos = line.find_first_of(' ');
    tmp.protocol = line.substr(0, pos);
    //cout << "Request 2: " << tmp.protocol << endl;
    return tmp;
};
