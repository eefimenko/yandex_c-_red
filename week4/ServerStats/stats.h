#pragma once

#include "http_request.h"

#include <string_view>
#include <map>
using namespace std;

class Stats {
public:
Stats():methods_names({"GET", "PUT", "POST", "DELETE", "UNKNOWN"}),
	uris_names({"/", "/order", "/product", "/basket", "/help", "unknown"})
    {
	for (const auto& m : methods_names)
	{
	    methods[string_view(m)] = 0;
	}

	for (const auto& u : uris_names)
	{
	    uris[string_view(u)] = 0;
	}
    };
    
    void AddMethod(string_view method)
    {
	methods[getMethodName(method)] += 1;
    };
    
    void AddUri(string_view uri)
    {
	uris[getUriName(uri)] += 1;
    };
    
    const map<string_view, int>& GetMethodStats() const {return methods;};
    const map<string_view, int>& GetUriStats() const {return uris;};
private:
    
    string_view getMethodName(string_view method)
    {
	for (size_t i = 0; i < methods_names.size()-1; i++)
	{
	    if (methods_names[i] == method)
	    {
		return string_view(methods_names[i]);
	    }
	}
	return string_view(methods_names.back());
    }

    string_view getUriName(string_view uri)
    {
	for (size_t i = 0; i < uris_names.size()-1; i++)
	{
	    if (uris_names[i] == uri)
	    {
		return string_view(uris_names[i]);
	    }
	}
	return string_view(uris_names.back());
    }
  
    map<string_view, int> methods;
    map<string_view, int> uris;
    array<string, 5> methods_names;
    array<string, 6> uris_names;
};

HttpRequest ParseRequest(string_view line);
