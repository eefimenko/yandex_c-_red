#include "test_runner.h"
#include <string>
#include <vector>

using namespace std;

class Translator {
public:
  void Add(string_view source, string_view target)
    {
	string s_source = string{source};
	string s_target = string{target};
	
	forwardData.insert(s_source);
	backwardData.insert(s_target);
	
	string_view sv_source = string_view(*forwardData.find(s_source));
	string_view sv_target = string_view(*backwardData.find(s_target));

	forwardDictionary[sv_source] = sv_target;
	backwardDictionary[sv_target] = sv_source;
    };

    string_view TranslateForward(string_view source) const
    {
	auto it = forwardData.find(string(source));
	if (it != forwardData.end())
	{
	    return forwardDictionary.at(*it);
	}
	else
	{
	    return string_view(string());
	}
    };
    
    string_view TranslateBackward(string_view target) const
    {
	auto it = backwardData.find(string(target));
	if (it != backwardData.end())
	{
	    return backwardDictionary.at(*it);
	}
	else
	{
	    return string_view(string());
	}	
    };

private:
    map<string_view, string_view> forwardDictionary;
    map<string_view, string_view> backwardDictionary;
    set<string> forwardData;
    set<string> backwardData;
};

void TestSimple() {
  Translator translator;
  translator.Add(string("okno"), string("window"));
  translator.Add(string("stol"), string("table"));

  ASSERT_EQUAL(translator.TranslateForward("okno"), "window");
  ASSERT_EQUAL(translator.TranslateBackward("table"), "stol");
  ASSERT_EQUAL(translator.TranslateBackward("stol"), "");
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestSimple);
  return 0;
}
