#pragma once
#include <algorithm>
#include <cstdlib>
#include <iostream>

using namespace std;

// Реализуйте шаблон SimpleVector
template <typename T>
class SimpleVector {
public:
    SimpleVector() : size_(0), capacity_(0), data(nullptr) {};

    explicit SimpleVector(size_t size) : size_(size), capacity_(size)
    {
	data = new T[size];
    };

    SimpleVector(const SimpleVector& other) :
    data(new T[other.Capacity()]),
	size_(other.Size()),
	capacity_(other.Capacity())
    {
	copy(other.begin(), other.end(), begin());
    }

    SimpleVector(SimpleVector&& other) :
    data(other.data), size_(other.size_), capacity_(other.capacity_)
    {
	other.data = nullptr;
	other.size_ = other.capacity_ = 0;
    }
    
    ~SimpleVector() {delete[] data;};

    T& operator[](size_t index){ return data[index]; };

    T* begin() {return data;};
    T* end() {return data + size_;};

    const T* begin() const {return data;};
    const T* end() const {return data + size_;};

    size_t Size() const {return size_; };
    size_t Capacity() const {return capacity_; };
    void PushBack(const T& value) {
	if (data == nullptr)
	{
	    data = new T[1];
	    *data = value;
	    capacity_ = 1;
	    size_ = 1;
	}
	else
	{
	    if (size_ == capacity_){
		capacity_ *= 2;
		T* tmp = new T[capacity_];
		copy(data, data + size_, tmp);
		delete[] data;
		data = tmp;
		
	    }
	    data[size_++] = value; 
	}
    };

    void operator=(const SimpleVector& other)
    {
	delete[] data;
	data = new T[other.Capacity()];
	size_ = other.Size();
	capacity_ = other.Capacity();
	copy(other.begin(), other.end(), begin());
    }

    void operator=(SimpleVector&& other)
    {
	delete[] data;
	data = other.data;
	size_ = other.size_;
	capacity_ = other.capacity_;
	other.data = nullptr;
	other.size_ = other.capacity_ = 0;
    }
private:
  // Добавьте поля для хранения данных вектора
    size_t size_;
    size_t capacity_;
    T* data;
};
