#include "test_runner.h"
#include <vector>
#include <future>

using namespace std;
template <typename Iterator>
class Page
{
public:
    Page(Iterator first_, Iterator last_) : first(first_), last(last_) {};
    size_t size() const {return last - first; };
    auto begin() const { return first; };
    auto end() const {return last; };
    
private:
    Iterator first, last;
};

template <typename Iterator>
class Paginator {
public:
    Paginator(Iterator first, Iterator last, size_t size)
    {
        Iterator curr = first;
        while (curr != last)
        {
            size_t psize = min(size, (size_t)distance(curr, last));
            pages.push_back({curr, next(curr, psize)});
            curr = next(curr, psize);
        }
    };
    size_t size() const {return pages.size(); };
    auto begin() { return pages.begin(); };
    auto end() { return pages.end(); };
private:
    vector<Page<Iterator>> pages;
};

template <typename C>
auto Paginate(C& c, size_t page_size) {
    return Paginator{c.begin(), c.end(), page_size};
}

template <typename Container>
int64_t CalculateMatrixPage(const Container& matrix, size_t row_start, size_t page_size)
{
    int64_t res = 0;
    
    for (const auto& row: matrix)
    {
	for (const auto& item: row)
	{
	    res += item;
	}
    }
    return res;
}

int64_t CalculateMatrixSum(const vector<vector<int>>& matrix) {
  // Реализуйте эту функцию
    int64_t result = 0;
    size_t first_row = 0;
    size_t page_size = 100;
    
    vector<future<int64_t>> data;

    for (auto page: Paginate(matrix, page_size))
    {
	data.push_back(async([page, first_row, page_size] { return CalculateMatrixPage(page, first_row, page_size); }));
    }

    for (auto& f: data)
    {
	result += f.get();
    }
    
    return result;
}

void TestCalculateMatrixSum() {
  const vector<vector<int>> matrix = {
    {1, 2, 3, 4},
    {5, 6, 7, 8},
    {9, 10, 11, 12},
    {13, 14, 15, 16}
  };
  ASSERT_EQUAL(CalculateMatrixSum(matrix), 136);
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestCalculateMatrixSum);
}
