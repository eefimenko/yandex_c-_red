#include "test_runner.h"
#include <algorithm>
#include <memory>
#include <vector>

using namespace std;

template <typename RandomIt>
void MergeSort(RandomIt range_begin, RandomIt range_end) {
  // Напишите реализацию функции,
  // не копируя сортируемые элементы
    if ((range_end - range_begin) < 2)
    {
	return;
    }
    
    vector<typename RandomIt::value_type> v(make_move_iterator(range_begin), make_move_iterator(range_end));
    size_t sub_size = (range_end - range_begin)/3;
    size_t sub_size2 = sub_size*2;
    
    MergeSort(begin(v), begin(v) + sub_size);
    MergeSort(begin(v) + sub_size, begin(v) + sub_size2);
    MergeSort(begin(v) + sub_size2, end(v));

    vector<typename RandomIt::value_type> tmp;
    merge(make_move_iterator(begin(v)),
	  make_move_iterator(begin(v) + sub_size),
	  make_move_iterator(begin(v) + sub_size),
	  make_move_iterator(begin(v) + sub_size2),
	  back_inserter(tmp)
	);

    merge(make_move_iterator(begin(tmp)),
	  make_move_iterator(end(tmp)),
	  make_move_iterator(begin(v) + sub_size2),
	  make_move_iterator(end(v)),
	  range_begin
	);
    
}

void TestIntVector() {
  vector<int> numbers = {6, 1, 3, 9, 1, 9, 8, 12, 1};
  MergeSort(begin(numbers), end(numbers));
  ASSERT(is_sorted(begin(numbers), end(numbers)));
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestIntVector);
  return 0;
}
