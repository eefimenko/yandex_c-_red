#include "test_runner.h"
#include "profile.h"

#include <map>
#include <string>
#include <algorithm>
#include <future>
#include <sstream>

using namespace std;

struct Stats {
    map<string, int> word_frequences;
    
    void operator += (const Stats& other)
    {
	for (auto& f: other.word_frequences)
	{
	    word_frequences[f.first] += f.second;
	}
    }

    void print()
    {
	for (auto& f: word_frequences)
	{
	    cout << f.first << ":" << word_frequences[f.first] << endl;
	}
	cout << endl;
    }
};

/*Stats ExploreLine(const std::set<std::string>& key_words, const std::string& line) 
{
    std::string_view line_view = line;
	Stats result;

	size_t pos = line_view.find_first_not_of(' ');
	line_view.remove_prefix(pos);

    while (pos != line_view.npos)
    {
		pos = line_view.find_first_of(' ');

		auto key = std::string(line_view.substr(0, pos));

        if (key_words.count(key) > 0)
        {
			++result.word_frequences[std::move(key)];
        }

        if (pos == line_view.npos)
            break;

		line_view.remove_prefix(pos);
		pos = line_view.find_first_not_of(' ');
		line_view.remove_prefix(pos);
    }

	return result;
}*/

Stats ExploreLine(const set<string>& key_words, const string& line) {
    Stats result;
    std::string_view sv = line;

    size_t pos = sv.find_first_not_of(' ');
    sv.remove_prefix(pos);

    while (pos != sv.npos)
    {
	size_t pos = sv.find_first_of(' ');
	string key = string(sv.substr(0,pos));

	if (key_words.count(key) > 0)
	{
	    result.word_frequences[move(key)]++;
	}

	if (pos == sv.npos)
	{
	    break;
	}

	sv.remove_prefix(pos);
	pos = sv.find_first_not_of(' ');
	sv.remove_prefix(pos);
    }
    
    return result;
}

Stats ExploreKeyWordsSingleThread(
  const set<string>& key_words, istream& input
) {
  Stats result;
  for (string line; getline(input, line); ) {
      // cout << line << endl;
    result += ExploreLine(key_words, line);
  }
  return result;
}

Stats ExploreKeyWords(const set<string>& key_words, istream& input) {
    Stats result;
    vector<future<Stats>> results;
    size_t num_threads = 8;
    vector<stringstream> lines(num_threads);

    size_t i = 0;
    
    for(string line; getline(input, line);)
    {
	lines[i%num_threads] << line << endl;
	i++;
    }
    //cout << string(lines) << endl;
    for (auto& l : lines)
    {
	results.push_back(async([&key_words, &l] { return ExploreKeyWordsSingleThread(key_words, l);}));
    }

    for (auto& r : results)
    {
	result += r.get();
    }
    
    return result;
}

void TestBasic() {
  const set<string> key_words = {"yangle", "rocks", "sucks", "all"};

  stringstream ss;
  ss << "this new yangle service really rocks\n";
  ss << "It sucks when yangle isn't available\n";
  ss << "10 reasons why yangle is the best IT company\n";
  ss << "yangle rocks others suck\n";
  ss << "Goondex really sucks, but yangle rocks. Use yangle\n";

  const auto stats = ExploreKeyWords(key_words, ss);
  const map<string, int> expected = {
    {"yangle", 6},
    {"rocks", 2},
    {"sucks", 1}
  };
  ASSERT_EQUAL(stats.word_frequences, expected);
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestBasic);
}
