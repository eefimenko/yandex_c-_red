#include "airline_ticket.h"
#include "test_runner.h"

#include <algorithm>
#include <numeric>
#include <iomanip>
#include <iostream>
using namespace std;

//#define SORT_BY(field)				 \
//[](const AirlineTicket& lhs, const AirlineTicket& rhs) {	\
//  return lhs.field < rhs.field;				\
//}

#define SORT_BY(field) [](const AirlineTicket& lhs, const AirlineTicket& rhs) { \
	return get_##field(lhs) < get_##field(rhs);			\
  }
// Реализуйте этот макрос, а также необходимые операторы для классов Date и Time

Time get_departure_time(const AirlineTicket& ticket)
{
    return ticket.departure_time;
}

Time get_arrival_time(const AirlineTicket& ticket)
{
    return ticket.arrival_time;
}

Date get_departure_date(const AirlineTicket& ticket)
{
    return ticket.departure_date;
}

Date get_arrival_date(const AirlineTicket& ticket)
{
    return ticket.arrival_date;
}

int get_price(const AirlineTicket& ticket)
{
    return ticket.price;
}

string get_to(const AirlineTicket& ticket)
{
    return ticket.to;
}

string get_from(const AirlineTicket& ticket)
{
    return ticket.from;
}

string get_airline(const AirlineTicket& ticket)
{
    return ticket.airline;
}

ostream& operator<<(ostream& out, const Date& date)
{
    out << setw(4) << setfill('0') << date.year << "-"
	<< setw(2) << setfill('0') << date.month << "-"
	<< setw(2) << setfill('0') << date.day;
    return out;
}

ostream& operator<<(ostream& out, const Time& time)
{
    out << setw(2) << setfill('0') << time.hours << "-"
	<< setw(2) << setfill('0') << time.minutes;
    return out;
}

int GetNumberOfDays(const Date& date)
{
    return date.year * 360 + (date.month - 1) * 30 + date.day;
}

int GetNumberOfMinutes(const Time& time)
{
    return time.hours * 60 + time.minutes;
}

bool operator<(const Date& lhs, const Date& rhs)
{
    return GetNumberOfDays(lhs) < GetNumberOfDays(rhs);
};

bool operator==(const Date& lhs, const Date& rhs)
{
    return GetNumberOfDays(lhs) == GetNumberOfDays(rhs);
};

bool operator<(const Time& lhs, const Time& rhs)
{
    return GetNumberOfMinutes(lhs) < GetNumberOfMinutes(rhs);
};

bool operator==(const Time& lhs, const Time& rhs)
{
    return GetNumberOfMinutes(lhs) == GetNumberOfMinutes(rhs);
};

void TestSortBy() {
  vector<AirlineTicket> tixs = {
    {"VKO", "AER", "Utair",     {2018, 2, 28}, {17, 40}, {2018, 2, 28}, {20,  0}, 1200},
    {"AER", "VKO", "Utair",     {2018, 3,  5}, {14, 15}, {2018, 3,  5}, {16, 30}, 1700},
    {"AER", "SVO", "Aeroflot",  {2018, 3,  5}, {18, 30}, {2018, 3,  5}, {20, 30}, 2300},
    {"PMI", "DME", "Iberia",    {2018, 2,  8}, {23, 00}, {2018, 2,  9}, { 3, 30}, 9000},
    {"CDG", "SVO", "AirFrance", {2018, 3,  1}, {13, 00}, {2018, 3,  1}, {17, 30}, 8000},
  };
  
  sort(begin(tixs), end(tixs), SORT_BY(price));
  ASSERT_EQUAL(tixs.front().price, 1200);
  ASSERT_EQUAL(tixs.back().price, 9000);

  sort(begin(tixs), end(tixs), SORT_BY(from));
  ASSERT_EQUAL(tixs.front().from, "AER");
  ASSERT_EQUAL(tixs.back().from, "VKO");

  sort(begin(tixs), end(tixs), SORT_BY(arrival_date));
  ASSERT_EQUAL(tixs.front().arrival_date, (Date{2018, 2, 9}));
  ASSERT_EQUAL(tixs.back().arrival_date, (Date{2018, 3, 5}));
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestSortBy);
}
