#include "airline_ticket.h"
#include "test_runner.h"
#include <iostream>
#include <iomanip>

using namespace std;

#define UPDATE_FIELD(ticket, field, values) \
    {					    \
	auto it = values.find(#field);	    \
	if (it != values.end()) {	    \
	    istringstream is(it->second);   \
	    is >> ticket.field;		    \
	}				    \
    }


// Реализуйте этот макрос, а также необходимые операторы для классов Date и Time

ostream& operator<<(ostream& out, const Date& date)
{
    out << setw(4) << setfill('0') << date.year << "-"
	<< setw(2) << setfill('0') << date.month << "-"
	<< setw(2) << setfill('0') << date.day;
    return out;
}

ostream& operator<<(ostream& out, const Time& time)
{
    out << setw(2) << setfill('0') << time.hours << "-"
	<< setw(2) << setfill('0') << time.minutes;
    return out;
}

istream& operator>>(istream& in, Time& time)
{
    in >> time.hours;
    in.ignore(1);
    in >> time.minutes;
    return in;
}

istream& operator>>(istream& in, Date& date)
{
    in >> date.year;
    in.ignore(1);
    in >> date.month;
    in.ignore(1);
    in >> date.day;
    return in;
}

int GetNumberOfDays(const Date& date)
{
    return date.year * 360 + (date.month - 1) * 30 + date.day;
}

int GetNumberOfMinutes(const Time& time)
{
    return time.hours * 60 + time.minutes;
}

bool operator<(const Date& lhs, const Date& rhs)
{
    return GetNumberOfDays(lhs) < GetNumberOfDays(rhs);
};

bool operator==(const Date& lhs, const Date& rhs)
{
    return GetNumberOfDays(lhs) == GetNumberOfDays(rhs);
};

bool operator<(const Time& lhs, const Time& rhs)
{
    return GetNumberOfMinutes(lhs) < GetNumberOfMinutes(rhs);
};

bool operator==(const Time& lhs, const Time& rhs)
{
    return GetNumberOfMinutes(lhs) == GetNumberOfMinutes(rhs);
};

void TestUpdate() {
  AirlineTicket t;
  t.price = 0;

  const map<string, string> updates1 = {
    {"departure_date", "2018-2-28"},
    {"departure_time", "17:40"},
  };
  UPDATE_FIELD(t, departure_date, updates1);
  UPDATE_FIELD(t, departure_time, updates1);
  UPDATE_FIELD(t, price, updates1);

  ASSERT_EQUAL(t.departure_date, (Date{2018, 2, 28}));
  ASSERT_EQUAL(t.departure_time, (Time{17, 40}));
  ASSERT_EQUAL(t.price, 0);

  const map<string, string> updates2 = {
    {"price", "12550"},
    {"arrival_time", "20:33"},
  };
  UPDATE_FIELD(t, departure_date, updates2);
  UPDATE_FIELD(t, departure_time, updates2);
  UPDATE_FIELD(t, arrival_time, updates2);
  UPDATE_FIELD(t, price, updates2);

  // updates2 не содержит ключей "departure_date" и "departure_time", поэтому
  // значения этих полей не должны измениться
  ASSERT_EQUAL(t.departure_date, (Date{2018, 2, 28}));
  ASSERT_EQUAL(t.departure_time, (Time{17, 40}));
  ASSERT_EQUAL(t.price, 12550);
  ASSERT_EQUAL(t.arrival_time, (Time{20, 33}));
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestUpdate);
}
