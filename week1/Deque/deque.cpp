//#include "test_runner.h"
#include <iostream>
#include <vector>
#include <exception>
#include <stdexcept>

using namespace std;

template<typename T>
class Deque
{
  public:
    Deque(){};
    
    size_t Size() const { return front.size() + back.size(); }
    
    bool Empty() const { return Size() == 0; }
    
    T& operator [] (size_t idx) {
	if (idx < front.size())
	{
	    return front[front.size() - idx - 1];
	}
	else
	{
	    return back[idx - front.size()];
	}
    }
    
    const T& operator [] (size_t idx) const {
        if (idx < front.size())
	{
	    return front.at(front.size() - idx - 1);
	}
	else
	{
	    return back.at(idx - front.size());
	}
    }
    
    T& At(size_t idx) {
	if (idx >= Size()) {
	    throw out_of_range("Out of bounds");
	}
	if (idx < front.size())
	{
	    return front[front.size() - idx - 1];
	}
	else
	{
	    return back[idx - front.size()];
	}
    }
    
    const T& At(size_t idx) const {
	if (idx >= Size()) {
	    throw out_of_range("Out of bounds");
	}
	if (idx < front.size())
	{
	    return front(front.size() - idx - 1);
	}
	else
	{
	    return back(idx - front.size());
	}
    }
    
    T& Front() {
	if (front.size() > 0)
	{
	    return front.back();
        }
	else
	{
	    size_t size = back.size()/2;
	    vector<T> local;
	    for (size_t i = 0; i < size; i++)
	    {
		local.push_back(back.back());
		back.pop_back();
	    }

	    while (!back.empty())
	    {
		front.push_back(back.back());
		back.pop_back();
	    }

	    while (!local.empty())
	    {
		back.push_back(local.back());
		local.pop_back();
	    }
	    return front.back();
	}
    }
    
    const T& Front() const {
	if (front.size() > 0)
	{
	    return front.back();
        }
	else
	{
	    return *back.begin();
	}
    }
    
    T& Back() {
	if (back.size() > 0)
	{
	    return back.back();
        }
	else
	{
	    size_t size = front.size()/2;
	    vector<T> local;
	    for (size_t i = 0; i < size; i++)
	    {
		local.push_back(front.back());
		front.pop_back();
	    }

	    while (!front.empty())
	    {
		back.push_back(front.back());
		front.pop_back();
	    }

	    while (!local.empty())
	    {
		front.push_back(local.back());
		local.pop_back();
	    }
	    return back.back();
	}
    }
    
    const T& Back() const {
	if (back.size() > 0)
	{
	    return back.back();
        }
	else
	{
	    return *front.begin();
	}
    }
    
    void PushBack(T t) { back.push_back(t); }
    
    void PushFront(T t) { front.push_back(t); }
        
  private:
    vector<T> front;
    vector<T> back;
};

template <typename T>
void Print(const Deque<T>& d)
{
    for (size_t i=0; i < d.Size(); i++)
    {
	cout << d[i] << " "; 
    }
    cout << endl;
}

int main() {
    Deque<int> deque;
    cout << deque.Size() << endl;
    deque.PushFront(2);
    deque.PushFront(1);
    deque.PushFront(0);
    deque.PushBack(3);
    deque.PushBack(4);
    deque.PushBack(5);
    cout << deque.Size() << endl;

    Print(deque); 
       
    cout << deque.Front() << endl;
    cout << deque.Back() << endl;
    
    Deque<int> deque2;
    deque2.PushBack(0);
    deque2.PushBack(1);
    deque2.PushBack(2);
    deque2.PushBack(3);
    deque2.PushBack(4);
    deque2.PushBack(5);
    cout << deque2.Front() << endl;
    cout << deque2.Back() << endl;

    Deque<int> deque3;
    deque3.PushFront(5);
    deque3.PushFront(4); 
    deque3.PushFront(3);
    deque3.PushFront(2);
    deque3.PushFront(1);
    deque3.PushFront(0);
    
    cout << deque3.Front() << endl;
    cout << deque3.Back() << endl;

    Deque<string> d;
    d.PushBack("A");
    cout << d.Front() << " " << d.Back() << " " << d.Size() << endl;
    d.Back() = "B";
    cout << d.Front() << " " << d.Back() << " " << d.Size() << endl;
    return 0;
}
