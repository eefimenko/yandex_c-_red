#pragma once

#include <vector>
#include <exception>
#include <stdexcept>

using namespace std;

template<typename T>
class Deque
{
  public:
    Deque(){};
    
    size_t Size() const { return front.size() + back.size(); }
    
    bool Empty() const { return Size() == 0; }
    
    T& operator [] (size_t idx) {
	if (idx < front.size())
	{
	    return front[front.size() - idx - 1];
	}
	else
	{
	    return back[idx - front.size()];
	}
    }
    
    const T& operator [] (size_t idx) const {
        if (idx < front.size())
	{
	    return front[front.size() - idx - 1];
	}
	else
	{
	    return back[idx - front.size()];
	}
    }
    
    T& At(size_t idx) {
	if (idx >= Size()) {
	    throw out_of_range("Out of bounds");
	}
	if (idx < front.size())
	{
	    return front[front.size() - idx - 1];
	}
	else
	{
	    return back[idx - front.size()];
	}
    }
    
    const T& At(size_t idx) const {
	if (idx >= Size()) {
	    throw out_of_range("Out of bounds");
	}
	if (idx < front.size())
	{
	    return front[front.size() - idx - 1];
	}
	else
	{
	    return back[idx - front.size()];
	}
    }
    
    T& Front() {
	if (front.size() > 0)
	{
	    return front.back();
        }
	else
	{
	    size_t size = back.size()/2;
	    vector<T> local;
	    for (size_t i = 0; i < size; i++)
	    {
		local.push_back(back.back());
		back.pop_back();
	    }

	    while (!back.empty())
	    {
		front.push_back(back.back());
		back.pop_back();
	    }

	    while (!local.empty())
	    {
		back.push_back(local.back());
		local.pop_back();
	    }
	    return front.back();
	}
    }
    
    const T& Front() const {
	if (front.size() > 0)
	{
	    return front.back();
        }
	else
	{
	    size_t size = back.size()/2;
	    vector<T> local;
	    for (size_t i = 0; i < size; i++)
	    {
		local.push_back(back.back());
		back.pop_back();
	    }

	    while (!back.empty())
	    {
		front.push_back(back.back());
		back.pop_back();
	    }

	    while (!local.empty())
	    {
		back.push_back(local.back());
		local.pop_back();
	    }
	    return front.back();
	}
    }
    
    T& Back() {
	if (back.size() > 0)
	{
	    return back.back();
        }
	else
	{
	    size_t size = front.size()/2;
	    vector<T> local;
	    for (size_t i = 0; i < size; i++)
	    {
		local.push_back(front.back());
		front.pop_back();
	    }

	    while (!front.empty())
	    {
		back.push_back(front.back());
		front.pop_back();
	    }

	    while (!local.empty())
	    {
		front.push_back(local.back());
		local.pop_back();
	    }
	    return back.back();
	}
    }
    
    const T& Back() const {
	if (back.size() > 0)
	{
	    return back.back();
        }
	else
	{
	    size_t size = front.size()/2;
	    vector<T> local;
	    for (size_t i = 0; i < size; i++)
	    {
		local.push_back(front.back());
		front.pop_back();
	    }

	    while (!front.empty())
	    {
		back.push_back(front.back());
		front.pop_back();
	    }

	    while (!local.empty())
	    {
		front.push_back(local.back());
		local.pop_back();
	    }
	    return back.back();
	}
    }
    
    void PushBack(T t) { back.push_back(t); }
    
    void PushFront(T t) { front.push_back(t); }
        
  private:
    vector<T> front;
    vector<T> back;
};
