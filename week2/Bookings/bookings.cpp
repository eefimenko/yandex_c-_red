#include "test_runner.h"
#include <algorithm>
#include <map>
#include <utility>
#include <iostream>
#include <string>
#include <set>
#include <queue>

using namespace std;

class Reservation
{
public:
    Reservation(int64_t time, const string& hotel_name, int client_id, int rooms_count) :
	time_(time),
	hotel_name_(hotel_name),
	client_id_(client_id),
	rooms_count_(rooms_count) {}

    inline int64_t getTime() const {return time_;}
    inline const string& getHotel() const {return hotel_name_;}
    inline int getClient() const {return client_id_;}
    inline int getRooms() const {return rooms_count_;}

    void print() const
    {
	cout << "Reservation at " << time_
	     << ". Hotel: "       << hotel_name_
	     << ". Client:  "     << client_id_
	     << ". Rooms: "       << rooms_count_
	     << endl;
    }
    
private:
    int64_t time_;
    string hotel_name_;
    int client_id_;
    int rooms_count_;
};

ostream& operator<<(ostream& s, const Reservation& r)
{
    s << "Reservation at " << r.getTime()
      << ". Hotel: "       << r.getHotel()
      << ". Client:  "     << r.getClient()
      << ". Rooms: "       << r.getRooms();
      
    return s;
}

class BookingManager {
public:
    BookingManager() : bookings(), rooms(), clients() {}

    void updateQueues()
    {
	while (!bookings.empty())
	{
	    auto& p = bookings.front();
	    int64_t time_ = p.getTime();
	    if (time_ <= current_time - PERIOD)
	    {
		int rooms_ = p.getRooms();
		int client_id_ = p.getClient();
		const string& hotel_name = p.getHotel();
		
		rooms[hotel_name] -= rooms_;
		auto& cl = clients[hotel_name];
		
		auto it = cl.find(client_id_);
		
		it->second -= rooms_;
		
		if (it->second == 0)
		{
		    cl.erase(it);
		}
		    
		bookings.pop(); // remove old booking from queue
	    }
	    else
	    {
		break; // found first not too old booking, nothing more to delete
	    }
	}
    }
    
    
    void bookRooms(int64_t time, const string& hotel_name, int client_id, int room_count)
    {
	current_time = time;
	bookings.push(Reservation(time, hotel_name, client_id, room_count));
	rooms[hotel_name] += room_count;
	clients[hotel_name][client_id] += room_count;
    }
    
    int countClients(const string& hotel_name)
    {
	updateQueues();
	return clients[hotel_name].size();
    }

    int countRooms(const string& hotel_name)
    {
	updateQueues();
	return rooms[hotel_name];	
    }
private:
    const static int64_t PERIOD = 86400; // sec
    int64_t current_time;
    queue<Reservation> bookings;
    map<string, int> rooms;
    map<string, map<int, int>> clients;
//    map<string, vector<int>> clients;
};

void test() {
    BookingManager manager;

    // Test empty bookings
    ASSERT_EQUAL(manager.countClients("mariott"), 0);
    ASSERT_EQUAL(manager.countRooms("mariott"), 0);

    manager.bookRooms(0, "mariott", 1, 10);
    manager.bookRooms(0, "mariott", 2, 1);
    manager.bookRooms(0, "mariott", 3, 1);
    manager.bookRooms(0, "mariott", 4, 1);
    manager.bookRooms(0, "hilton", 1, 1);
    manager.bookRooms(1, "hilton", 2, 1);

    // Test correctness
    ASSERT_EQUAL(manager.countClients("mariott"), 4);
    ASSERT_EQUAL(manager.countRooms("mariott"), 13);
    ASSERT_EQUAL(manager.countClients("hilton"), 2);
    ASSERT_EQUAL(manager.countRooms("hilton"), 2);

    // Test event past 1 day resets statics
    manager.bookRooms(86400, "mariott", 1, 1);
    ASSERT_EQUAL(manager.countClients("mariott"), 1);
    ASSERT_EQUAL(manager.countRooms("mariott"), 1);
    ASSERT_EQUAL(manager.countClients("hilton"), 1);
    ASSERT_EQUAL(manager.countRooms("hilton"), 1);

    // Test no clients and room for the last day
    manager.bookRooms(86401, "mariott", 5, 1);
    ASSERT_EQUAL(manager.countClients("mariott"), 2);
    ASSERT_EQUAL(manager.countRooms("mariott"), 2);
    ASSERT_EQUAL(manager.countClients("hilton"), 0);
    ASSERT_EQUAL(manager.countRooms("hilton"), 0);
}


int main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);

//    test();
//    return 0;
    
    BookingManager manager;
    
    int query_count;
    cin >> query_count;

    for (int query_id = 0; query_id < query_count; ++query_id) {
	string query_type;
	cin >> query_type;
	
	if (query_type == "BOOK") {
	    int64_t time;
	    int client_id, room_count;
	    string hotel_name;
            cin >> time >> hotel_name >> client_id >> room_count;
	    manager.bookRooms(time, hotel_name, client_id, room_count);
	}
	else if (query_type == "CLIENTS")
	{
	    string hotel_name;
	    cin >> hotel_name;
	    cout << manager.countClients(hotel_name) << "\n";
	}
	else if (query_type == "ROOMS")
	{
	    string hotel_name;
	    cin >> hotel_name;
	    cout << manager.countRooms(hotel_name) << "\n";
	}
    }     
    
    return 0;
}
