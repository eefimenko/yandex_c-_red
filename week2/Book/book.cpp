#include <iomanip>
#include <iostream>
#include <vector>
#include <utility>
#include <map>

using namespace std;

class ReadingManager {
public:
  ReadingManager()
      : user_page_counts_(),
        pages_for_user_() {}

  void Read(int user_id, int page_count) {
      // Check if we already added this user
      auto it = user_page_counts_.find(user_id);
      
      if (it != user_page_counts_.end())
      {
	  int prev_pages_count = it->second;
	  auto it_ = pages_for_user_.find(prev_pages_count);
	  if (it_->second == 1)
	  {
	      pages_for_user_.erase(it_);
	  }
	  else
	  {
	      it_->second--;
	  }
	  it->second = page_count;
      }
      else
      {
	  user_page_counts_[user_id] = page_count;
      }
      pages_for_user_[page_count]++;
//      cout << "*After Read " << user_id << " " << page_count<< endl;
//      print();
  }

  double Cheer(int user_id) const {
//      cout << "*In Cheer " << user_id << endl;
//      print();
      auto it = user_page_counts_.find(user_id);
      if (it == user_page_counts_.end()) {
	  return 0;
      }
      if (user_page_counts_.size() == 1)
      {
	  return 1;
      }
      int page_count = it->second;

      auto it_ = pages_for_user_.lower_bound(page_count);
      int users = 0;
      auto it__ = pages_for_user_.begin();
      while (it__ != it_)
      {
	  users += it__->second;
	  it__ = next(it__);
      }
//      users += pages_for_user_.find(page_count)->second - 1;
     
      return 1.0 * users/(user_page_counts_.size()-1);
  }

private:
  // Статическое поле не принадлежит какому-то конкретному
  // объекту класса. По сути это глобальная переменная,
  // в данном случае константная.
  // Будь она публичной, к ней можно было бы обратиться снаружи
  // следующим образом: ReadingManager::MAX_USER_COUNT.
  static const int MAX_USER_COUNT_ = 100000;

    map<int, int> user_page_counts_;
    map<int, int> pages_for_user_; // count of users with this number of pages

    void print() const
    {
	cout << "*User page counts"<< endl;
	for (const auto& p : user_page_counts_)
	{
	    cout <<"*"<< p.first << " " << p.second << endl;
	}
	cout << "*Pages for user"<< endl;
	for (const auto& p : pages_for_user_)
	{
	    cout <<"*"<< p.first << " " << p.second << endl;
	}
    }
//  vector<int> user_page_counts_;
//  vector<int> sorted_users_;   // отсортированы по убыванию количества страниц
//  vector<int> user_positions_; // позиции в векторе sorted_users_

/*  int GetUserCount() const {
    return sorted_users_.size();
  }
  void AddUser(int user_id) {
    sorted_users_.push_back(user_id);
    user_positions_[user_id] = sorted_users_.size() - 1;
  }
  void SwapUsers(int lhs_position, int rhs_position) {
    const int lhs_id = sorted_users_[lhs_position];
    const int rhs_id = sorted_users_[rhs_position];
    swap(sorted_users_[lhs_position], sorted_users_[rhs_position]);
    swap(user_positions_[lhs_id], user_positions_[rhs_id]);
    }*/
};


int main() {
  // Для ускорения чтения данных отключается синхронизация
  // cin и cout с stdio,
  // а также выполняется отвязка cin от cout
  ios::sync_with_stdio(false);
  cin.tie(nullptr);

  ReadingManager manager;

  int query_count;
  cin >> query_count;

  for (int query_id = 0; query_id < query_count; ++query_id) {
    string query_type;
    cin >> query_type;
    int user_id;
    cin >> user_id;
//    cout << query_type << " " << user_id << endl;
    if (query_type == "READ") {
      int page_count;
      cin >> page_count;
      manager.Read(user_id, page_count);
    } else if (query_type == "CHEER") {
      cout << setprecision(6) << manager.Cheer(user_id) << "\n";
    }
//    cout << "\n"<< endl;
  }

  return 0;
}
