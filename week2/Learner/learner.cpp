#include <algorithm>
#include <string>
#include <vector>
#include <set>

using namespace std;

class Learner {
 private:
  set<string> dict;

 public:
/*  int Learn(const vector<string>& words) {
    int newWords = 0;
    for (const auto& word : words) {
      if (find(dict.begin(), dict.end(), word) == dict.end()) {
        ++newWords;
        dict.push_back(word);
      }
    }
    return newWords;
    }*/
   int Learn(const vector<string>& words) {
       int newWords = 0;
       for (const auto& w : words)
       {
	   if (dict.find(w) == dict.end())
	   {
	       newWords++;
	       dict.insert(w);
	   }
       }
       
       return newWords;
    }

  vector<string> KnownWords() {
                 
      return vector<string>{dict.begin(), dict.end()};
  }
};
